package cellular;
import java.util.Objects;
import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;


public class BriansBrain implements CellAutomaton {
    IGrid currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		// TODO
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		// TODO
		return currentGeneration.numColumns() ;
	}

	@Override
	public CellState getCellState(int row, int col) {
		// TODO
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		// TODO
		for(int row = 0; row < numberOfRows(); row++){
			for(int col = 0; col < numberOfColumns(); col++){
				nextGeneration.set(row,col,getNextCell(row,col));
			}
		}
		currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		// TODO
		CellState cellCurrent = getCellState(row, col);
		int neighbours = countNeighbors(row, col, CellState.ALIVE);

		CellState returnCell = CellState.ALIVE;

		//if(cellCurrent == CellState.ALIVE){
		    if (cellCurrent == CellState.ALIVE){returnCell = CellState.DYING;}
			if (cellCurrent == CellState.DYING){returnCell = CellState.DEAD;}
			if(cellCurrent == CellState.DEAD && neighbours==2){returnCell = CellState.ALIVE;}

			//if(neighbours==2){returnCell = CellState.ALIVE;}
			//if(neighbours==3){returnCell = CellState.ALIVE;}
			//if(neighbours <2){returnCell = CellState.DYING;}
			//if(neighbours <2){returnCell = CellState.DEAD;}
			//if(neighbours >3){returnCell = CellState.DYING;}
			//if(neighbours >3){returnCell = CellState.DEAD;}
		
		else{
			if(cellCurrent == CellState.DEAD){returnCell= CellState.DEAD;} 
		}
		return returnCell;
		

		/** 
		if (cellCurrent == CellState.ALIVE && neighbours < 2){
			return CellState.DEAD;
		}
		if ((neighbours == 2) || (neighbours ==3 )){
			return CellState.ALIVE;
		}
		if (neighbours > 3){
			return CellState.DEAD;
		}
		return CellState.ALIVE;
		*/
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
        // TODO
        int counter = 0;
        for (int r = row -1; r <=row+1; r++){
			if(r < 0) continue;
			if (r >= currentGeneration.numRows()) break; 
            for(int c = col -1; c <= col+1; c++){
                if ( c < 0) continue;
				if (c >= currentGeneration.numColumns()) break;
				if ( r == row && c == col) continue;
				if ( currentGeneration.get(r, c).equals(state)) counter++;
                }
			}
			return counter;
		}
        @Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}

    

