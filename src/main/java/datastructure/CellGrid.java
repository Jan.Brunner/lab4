package datastructure;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Optional;

import javax.lang.model.element.Element;
import javax.swing.text.Position;

import cellular.CellState;

public class CellGrid implements IGrid {
    int rows;
    int cols;
    ArrayList<CellState> grid;
   // ArrayList<CellState> grid2;
     

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
       this.rows = rows;
       this.cols = columns;
       grid = new ArrayList<CellState>(); 
       for (int row = 0; row < rows; row++){
           for (int col=0; col < cols; col++ ){ 
            grid.add(initialState); 
           }
       }
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        if (row >= 0 && row < numRows()){
            if (column >= 0 && column < numColumns()){
                int pos = column + row*cols;
                this.grid.set(pos, element);
            } else {
                throw new IndexOutOfBoundsException();
            }
        }else {
            throw new IndexOutOfBoundsException();
        } 
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        if (row >= 0 && row < numRows()){
            if (column >= 0 && column < numColumns()){
                int pos = column + row*cols;
                return this.grid.get(pos);
            } else {
                throw new IndexOutOfBoundsException();
            }
        }else {
            throw new IndexOutOfBoundsException();
        } 
    }
    

    @Override
    public IGrid copy() {
        var newGrid = new CellGrid(this.numRows(), this.numColumns(), null);
        for(int row = 0; row < rows; row++)
        {
            for(int col = 0; col < cols; col++)
           {
                newGrid.set(row, col, this.get(row, col));
            }
        }


        /*for(CellState i: this.grid){
            int col = 0;
            for (int row = 0; row < rows; row++){
                newGrid.set(row, col, i);
                col++;
            }
        }
        */
        //newGrid.grid = this.grid;
        return newGrid;
        }
    }